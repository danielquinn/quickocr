# QuickOCR

A simple web app to help you OCR documents quickly


# Running in production

QuickOCR doesn't make use of a database or anything, so all you need to do is
run the docker container:

```bash
$ docker run --name quickocr -p 8000:8000 \
  -v /var/www/www.quickocr.com/:/app/ \
  -d danielquinn/quickocr
```

And then configure Nginx to forward QuickOCR traffic to localhost:8000:

```
upstream quickocr_server {
  server 127.0.0.1:8000;
}

server {

  ...

  location / {
    try_files $uri @proxy_to_app;
  }

  location @proxy_to_app {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto https;
    proxy_set_header Host $host;
    proxy_redirect off;
    proxy_pass http://quickocr_server;
  }

}
```

