import os
import re
import shutil
import subprocess
import tempfile

import pyocr
import pyocr.builders

from PIL import Image

from django import forms
from django.views.generic import FormView


class IndexForm(forms.Form):

    LANGUAGES = [
        ("afr", "Afrikaans"),
        ("ara", "Arabic"),
        ("aze", "Azerbaijani"),
        ("bel", "Belarusian"),
        ("ben", "Bengali"),
        ("bul", "Bulgarian"),
        ("cat", "Catalan"),
        ("ces", "Czech"),
        ("chi-sim", "Chinese (sim)"),
        ("chi-tra", "Chinese (tra)"),
        ("dan", "Danish"),
        ("deu", "German"),
        ("ell", "Greek"),
        ("eng", "English"),
        ("epo", "Esperanto"),
        ("est", "Estonian"),
        ("eus", "Basque"),
        ("fin", "Finnish"),
        ("fra", "French"),
        ("glg", "Galician"),
        ("heb", "Hebrew"),
        ("hin", "Hindi"),
        ("hrv", "Croatian"),
        ("hun", "Hungarian"),
        ("ind", "Indonesian"),
        ("isl", "Icelandic"),
        ("ita", "Italian"),
        ("jpn", "Japanese"),
        ("kan", "Kannada"),
        ("kor", "Korean"),
        ("lav", "Latvian"),
        ("lit", "Lithuanian"),
        ("mal", "Malayalam"),
        ("mkd", "Macedonian"),
        ("mlt", "Maltese"),
        ("msa", "Malay"),
        ("nld", "Dutch"),
        ("nor", "Norwegian"),
        ("pol", "Polish"),
        ("por", "Portuguese"),
        ("ron", "Romanian"),
        ("rus", "Russian"),
        ("slk", "Slovak"),
        ("slv", "Slovene"),
        ("spa", "Spanish"),
        ("sqi", "Albanian"),
        ("srp", "Serbian"),
        ("swa", "Swahili"),
        ("swe", "Swedish"),
        ("tam", "Tamil"),
        ("tel", "Telugu"),
        ("tgl", "Tagalog"),
        ("tha", "Thai"),
        ("tur", "Turkish"),
        ("ukr", "Ukrainian"),
        ("vie", "Vietnamese"),
    ]

    file = forms.FileField()
    language = forms.ChoiceField(
        choices=tuple([l for l in sorted(LANGUAGES, key=lambda _: _[1])]))

    def __init__(self, *args, **kwargs):

        forms.Form.__init__(self, *args, **kwargs)

        choices = tuple(self.fields["language"].choices)
        self.fields["language"].choices = (("eng", "English"),) + choices

        self.tesseract = pyocr.get_available_tools()[0]
        self._tmpdir = None
        self._original = None

    def complete(self):

        self._tmpdir = tempfile.mkdtemp(prefix="quickocr")
        self._original = os.path.join(self._tmpdir, "original")

        self._write_original()
        self._convert_to_pnm()
        self._unpaper()

        text = self._ocr()

        self._cleanup()

        return text

    def _write_original(self):
        with open(self._original, "wb") as f:
            f.write(self.cleaned_data["file"].read())

    def _convert_to_pnm(self):
        subprocess.Popen((
            "convert",
            "-density", "300",
            "-depth", "8",
            "-type", "grayscale",
            self._original,
            os.path.join(self._tmpdir, "convert-%04d.pnm")
        )).wait()

    def _unpaper(self):
        for f in os.listdir(self._tmpdir):
            if f.endswith(".pnm"):
                f = os.path.join(self._tmpdir, f)
                subprocess.Popen(
                    ("unpaper", f, f.replace(".pnm", ".unpaper.pnm"))).wait()

    def _ocr(self):
        r = ""
        for f in os.listdir(self._tmpdir):
            if f.endswith(".unpaper.pnm"):
                with Image.open(os.path.join(self._tmpdir, f)) as image:
                    r += re.sub(
                        r"\n+\s*\n+",
                        "\n",
                        self.tesseract.image_to_string(
                            image,
                            lang=self.cleaned_data["language"],
                            builder=pyocr.builders.TextBuilder()
                        ),
                        re.MULTILINE
                    ).strip()
        return r

    def _cleanup(self):
        shutil.rmtree(self._tmpdir)


class IndexFormView(FormView):

    form_class = IndexForm
    template_name = "ocr/index.html"

    def form_valid(self, form):
        context = self.get_context_data()
        context["ocr_output"] = form.complete()
        context["ocr_language"] = form.cleaned_data["language"]
        return self.render_to_response(context)

